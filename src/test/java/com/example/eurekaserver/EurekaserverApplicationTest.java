package com.example.eurekaserver;

import com.example.eurekaserver.EurekaserverApplication;
import org.junit.jupiter.api.Test;

/**
 * The type Springcloud gateway application test.
 */
class EurekaserverApplicationTest {

    /**
     * Main.
     */
    @Test
    void main() {
        EurekaserverApplication.main(new String[] {});

    }
}